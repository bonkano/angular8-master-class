import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { Department } from '../models/department-model';

@Injectable({
  providedIn: 'root'
})
export class DepartmentService {

  readonly APIUrl = "http://localhost:8080/api";
  formData: Department;

  constructor(private http: HttpClient) { }

  getDepList(): Observable<Department[]> {
    return this.http.get<Department[]>(this.APIUrl + '/departments');
  }

  addDepartment(dep: Department) {
    return this.http.post(this.APIUrl + '/departments', dep)
  }

  deleteDepartment(id: number) {
    return this.http.delete(this.APIUrl + '/departments/' + id);
  }

  updateDepartment(dep: Department, id: number) {
    id = dep.departmentID;
    return this.http.put(this.APIUrl + '/departments/' + id, dep);
  }


  private _listners = new Subject<any>();
  listen(): Observable<any> {
    return this._listners.asObservable();
  }
  filter(filterBy: string) {
    this._listners.next(filterBy);
  }
}
