import { Injectable } from '@angular/core';
import { Employee } from '../models/employee-model';
import { Observable, Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Department } from '../models/department-model';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private http: HttpClient) { }

  formData: Employee;

  readonly APIUrl = "http://localhost:8080/api";

  getEmpList(): Observable<Employee[]> {
    return this.http.get<Employee[]>(this.APIUrl + '/employees');
  }

  addEmployee(emp: Employee) {
    return this.http.post(this.APIUrl + '/employees', emp)
  }

  deleteEmployee(id: number) {
    return this.http.delete(this.APIUrl + '/employees/' + id);
  }

  updateEmployee(emp: Employee) {
    return this.http.put(this.APIUrl + '/employees/' + emp.employeeID, emp);
  }

  getDepDropDownValues(): Observable<any> {
    return this.http.get<Department[]>(this.APIUrl + '/departments');
  }


  private _listners = new Subject<any>();
  listen(): Observable<any> {
    return this._listners.asObservable();
  }
  filter(filterBy: string) {
    this._listners.next(filterBy);
  }
}
