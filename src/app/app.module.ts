import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {MatTableModule, MatIconModule, MatButtonModule, MatSortModule, MatDialogModule, MatSnackBarModule, MatDatepickerModule, MatNativeDateModule} from '@angular/material';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatInputModule} from '@angular/material/input';
import { EmployeeComponent } from './employee/employee.component';
import { DepartementComponent } from './departement/departement.component';
import { ShowEmpComponent } from './employee/show-emp/show-emp.component';
import { EditEmpComponent } from './employee/edit-emp/edit-emp.component';
import { AddEmpComponent } from './employee/add-emp/add-emp.component';
import { ShowDepComponent } from './departement/show-dep/show-dep.component';
import { EditDepComponent } from './departement/edit-dep/edit-dep.component';
import { AddDepComponent } from './departement/add-dep/add-dep.component';
import { EmployeeService } from './services/employee.service';
import { DepartmentService } from './services/departement.service';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AppComponent,
    EmployeeComponent,
    DepartementComponent,
    ShowEmpComponent,
    EditEmpComponent,
    AddEmpComponent,
    ShowDepComponent,
    EditDepComponent,
    AddDepComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    HttpClientModule,
    MatSortModule, MatDialogModule, 
    FormsModule,
    MatSnackBarModule,
    MatDatepickerModule,
    MatNativeDateModule
  ],
  providers: [EmployeeService, DepartmentService],
  bootstrap: [AppComponent],
  entryComponents: [AddDepComponent, EditDepComponent,AddEmpComponent, EditEmpComponent]
})
export class AppModule { }
