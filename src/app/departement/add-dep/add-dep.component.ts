import { Component, OnInit } from '@angular/core';
import { DepartmentService } from 'src/app/services/departement.service';
import { NgForm } from '@angular/forms';
import { MatDialogRef, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-add-dep',
  templateUrl: './add-dep.component.html',
  styleUrls: ['./add-dep.component.css']
})
export class AddDepComponent implements OnInit {

  constructor(private service: DepartmentService,
    public dialogbox: MatDialogRef<AddDepComponent>,
    private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.resetForm();
  }

  resetForm(form?: NgForm) {
    if (form != null) {
      form.resetForm();

      this.service.formData = { departmentID: 0, departmentName: '' };
    }
  }

  onClose() {
    this.dialogbox.close();
    this.service.filter('Register click');
  }

  onSubmit(form: NgForm) {
    this.service.addDepartment(form.value).subscribe(res => {
      this.resetForm(form);
      this.snackBar.open('Added Successfuly', '', {
        duration: 5000,
        verticalPosition: 'top'
      });
    }
    )
  }

}
